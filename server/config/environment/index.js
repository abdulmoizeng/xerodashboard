/**
 * Created by abdulmoiz on 1/23/2016.
 */
'use strict';

var path = require('path');

// All configurations will extend these options
// ============================================
var config = {
    env: process.env.NODE_ENV,

    // Root path of server
    root: path.normalize(__dirname + '/../../../'),

    // Server port
    port: process.env.PORT || 3000,

    xero_consumer_key : 'XWV8OBX9TRTTTQLT0NDCAP6RMAQ8IO',//my app consumer key

    xero_consumer_secret : 'FQJDPH2Y0YL8EI9XM3LXVP9SJAZMKD',//my app consumer secret

    xero_end_point_url   : 'https://api.xero.com/api.xro/2.0/',

    xero_private_key     : path.normalize(__dirname + '/../xero/privatekey.pem')//my app private key
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = config;
