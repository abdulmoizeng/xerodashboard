/**
 * Created by abdulmoiz on 1/31/2016.
 */
'use strict';

var express = require('express');
var XeroCtrl = require('./xero.controller.js');
var router = express.Router();

/*Exposing accounts api*/
router.get('/accounts', function (req, res) {
    XeroCtrl.getAccounts(req.query.endDate).then(function(data){
        res.send({
            success: true,
            data: data,
            message: 'Accounts fetched successfully!'
        });
    },function(err){
        res.status(err.status || 400).send(err);
    });
});

/*Exposing invoices api*/
router.get('/invoices', function (req, res) {
        XeroCtrl.getInvoices(req.query.startDate, req.query.endDate).then(function(invoices){
        res.send({
            success: true,
            data: invoices,
            message: 'Invoices info fetched successfully!'
        });
    },function(err){
        res.status(err.status || 400).send(err);
    });
});

/*Exposing payments api*/
router.get('/payments', function (req, res) {
    XeroCtrl.getPayments(req.query.startDate, req.query.endDate).then(function(data){
        res.send({
            success: true,
            data: data,
            message: 'Payments info fetched successfully!'
        });
    },function(err){
        res.status(err.status || 400).send(err);
    });
});

module.exports = router;
