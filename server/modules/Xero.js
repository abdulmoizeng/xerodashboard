/**
 * Created by abdulmoiz on 1/24/2016.
 */

var async = require('async');
var moment = require('moment')
var path = require('path');
var fs = require('fs');
var queryString = require('querystring');
var Xero = require('xero');
var config  = require('../config/environment');
var xero = new Xero(config.xero_consumer_key, config.xero_consumer_secret,
    fs.readFileSync(config.xero_private_key)/*, undefined,
    {'If-Modified-Since':'2016-01-26T19:03:08'}//start date when account is created*/
);

//if don't want to see system accounts just un comment above code with correct start date of  your xero organization


var XeroService = (function(){

    var XeroService = function(){

    };

    XeroService.prototype.getActiveAccounts = function(callback){
        var api = '/Accounts?'+queryString.stringify({where: 'Status="ACTIVE"'});
        //filter example 'Status="ACTIVE"&SystemAccount!=null'
        xero.call('GET', api, {}/*Body data*/, function(err, data) {
            if (err) {
                callback(err);
            }
            callback(null, data);
        });
    };

    XeroService.prototype.getPayments = function(startDate, endDate, callback){
        //filter example : Date >= DateTime(2015, 12, 1) && Date < DateTime(2015, 12, 28)
        //Payments
        var format = 'YYYY, MM, DD';/*'YYYY, MM, DD'*/
        var condition = '';
            if(startDate){
                condition += 'Date >= DateTime('+moment(startDate).format(format)+')';
            }
            if(startDate && endDate){
                condition +=' && ';
            }
            if(endDate){
                condition += 'Date < DateTime('+moment(endDate).format(format)+')';
            }

        var api = '/Payments?'+queryString.stringify({where: condition});
        xero.call('GET', api, {}/*form data*/, function(err, data) {
            if (err) {
                callback(err);
            }
            callback(null, data);
        });
    };

    XeroService.prototype.getBalanceSheet = function(date, callback){
        var api = '/Reports/BalanceSheet?'+queryString.stringify({
                date:moment(date).format('YYYY-MM-DD')
            });
        xero.call('GET', api, {}/*form data*/, function(err, data) {
            if (err) {
                callback(err);
            }
            callback(null, data);
        });
    };

    XeroService.prototype.getInvoices = function(startDate, endDate, callback){
        //filter example : Date >= DateTime(2015, 12, 1) && Date < DateTime(2015, 12, 28)
        //Invoices
        var format = 'YYYY, MM, DD';/*'YYYY, MM, DD'*/
        var condition = '';
        if(startDate){
            condition += 'Date >= DateTime('+moment(startDate).format(format)+')';
        }
        if(startDate && endDate){
            condition +=' && ';
        }
        if(endDate){
            condition += 'Date < DateTime('+moment(endDate).format(format)+')';
        }

        var api = '/Invoices?'+queryString.stringify({where: condition});

        xero.call('GET', api, {}/*form data*/, function(err, data) {
            if (err) {
                callback(err);
            }
            callback(null, data);
        });
    };

    var xeroInstance = new XeroService();
    return {
        getActiveAccounts          : xeroInstance.getActiveAccounts,
        getInvoices                : xeroInstance.getInvoices,
        getPayments                : xeroInstance.getPayments,
        getBalanceSheet            : xeroInstance.getBalanceSheet
    }
})();

module.exports = XeroService;

