/**
 * Created by abdulmoiz on 1/31/2016.
 */
'use strict';
angular.module('XeroApp')
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider

        /***** Routes *****/

            .state('dashboard', {
                url: '/',
                views: {
                    'container@': {
                        templateUrl: 'app/views/dashboard.html',
                        controller: 'DashboardCtrl'
                    }
                }
            })


    });
