/**
 * Created by abdulmoiz on 1/31/2016.
 */

module.exports = {
    /*For getting id to Balance mapping obejct from balance sheet response*/
    'extractBalance': function(response){
        var IdToBalanceMapping = {};
        try{
            var accounts = response.Reports.Report.Rows.Row[2].Rows.Row;
            for(var i = 0 ;i< (accounts.length-1) ; i++){
                var accountId = accounts[i].Cells.Cell[1].Attributes.Attribute.Value;
                var accountBalance = accounts[i].Cells.Cell[1].Value;
                IdToBalanceMapping[accountId] = accountBalance;
            }
        }
        catch(error){
            console.log(error);
        }
        return IdToBalanceMapping;
    }
};
