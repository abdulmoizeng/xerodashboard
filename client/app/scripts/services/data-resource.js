/**
 * Created by abdulmoiz on 2/2/2016.
 */
'use strict';
(function () {

    angular.module('XeroApp')
        .factory('XeroData', function ($resource) {
            return $resource('/api/xero',
                {},
                {
                    getAccountsInfo: {
                        url: '/api/xero/accounts',//api for fetching accounts
                        method:'GET',
                        endDate: '@_endDate'
                    },
                    getPaymentInfo: {
                        url: '/api/xero/payments',//api for fetching payments
                        method:'GET',
                        startDate: '@_startDate',
                        endDate: '@_endDate'
                    },
                    getInvoicesInfo: {
                        url: '/api/xero/invoices',//api for fetching invoices
                        method:'GET',
                        startDate: '@_startDate',
                        endDate: '@_endDate'
                    }
                }
            );

        })
})();
