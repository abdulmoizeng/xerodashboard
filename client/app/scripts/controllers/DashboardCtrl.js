/**
 * Created by abdulmoiz on 1/31/2016.
 */
'use strict';

angular.module('XeroApp')
    .controller('DashboardCtrl', function ($scope, XeroData, $location) {

        console.log('Dashboard ctrl');
        $scope.goTo = function(id){
          $location.hash(id);
        };

        function initializeData(){
            XeroData.getAccountsInfo({endDate: $scope.endDate}).$promise.then(
                function(resp){
                    $scope.accounts = resp.data;
                },
                function(resp){
                    //handle error response here
                    console.log('Error while fetching accounts!');
                }
            );

            XeroData.getInvoicesInfo({startDate: $scope.startDate,endDate: $scope.endDate}).$promise.then(
                function(resp){
                    $scope.invoices = resp.data;
                },
                function(resp){
                    //handle error response here
                    console.log('Error while fetching invoices!');
                }
            );

            XeroData.getPaymentInfo({startDate: $scope.startDate,endDate: $scope.endDate}).$promise.then(
                function(resp){
                    var payments = resp.data;
                    $scope.paymentCount = payments.length;
                    var typeToPriceMapping = {
                        'ACCRECPAYMENT':0,
                        'ACCPAYPAYMENT':0,
                        'ARCREDITPAYMENT':0,
                        'APCREDITPAYMENT':0,
                        'AROVERPAYMENTPAYMENT':0,
                        'ARPREPAYMENTPAYMENT':0,
                        'APPREPAYMENTPAYMENT':0,
                        'APOVERPAYMENTPAYMENT':0
                    }
                    payments.forEach(
                        function(payment){
                            typeToPriceMapping[payment['PaymentType']]+= parseFloat(payment['Amount']);
                        }

                    );
                    $scope.labels = ['ACCRECPAYMENT','ACCPAYPAYMENT','ARCREDITPAYMENT','APCREDITPAYMENT',
                                     'AROVERPAYMENTPAYMENT','ARPREPAYMENTPAYMENT','APPREPAYMENTPAYMENT',
                                     'APOVERPAYMENTPAYMENT'];
                    $scope.data = [];
                    $scope.labels.forEach(
                        function(label){
                            $scope.data.push(typeToPriceMapping[label]);
                        }
                    );

                },
                function(resp){
                    //handle error response here
                    console.log('Error while fetching payments!');
                }
            );
        }

        $scope.initData = initializeData;
        initializeData();




    });