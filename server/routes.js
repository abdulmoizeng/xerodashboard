/**
 * Created by abdulmoiz on 1/23/2016.
 */
/**
 * Main application routes
 */

'use strict';

var path = require('path');
var config = require('./config/environment');

module.exports = function(app) {

    //API routes
    app.use('/api/xero', require('./api/xero'));

    app.use(function(err, req, res, next) {
        res.status(500).send({
            success: false,
            message: 'Internal Server error !!',
            data: null,
            error: err
        });
    });



    // All other routes should redirect to the index.html
    require('./modules/Xero');



    app.route('/*')
        .get(function(req, res) {
            res.sendFile(path.resolve( config.root+ app.get('appPath') +'/app/index.html'));
        });
};
