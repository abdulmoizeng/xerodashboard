/**
 * Created by abdulmoiz on 2/2/2016.
 */
'use strict';
(function () {
    angular.module('XeroApp')
        /*Directive for dateRange Picker component*/
        .directive('dateRangePicker', function ($timeout) {
            return ({
                restrict: 'A',
                scope:{
                    startDate: '=',//= changes in scope variables any where reflect in both controller and directive
                    endDate  : '=',
                    initData : '='
                },
                link: function (scope, element) {
                    var date = new Date();
                    var MonthDays = [31, date.getYear()%4==0?29:28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];//default month days
                    scope.startDate = date.setDate(date.getDate()-(date.getDay()-1));//setting current month start date
                    var date = new Date();
                    scope.endDate   = date.setDate(date.getDate()-(date.getDay())+MonthDays[date.getMonth()]);//setting current month end date
                    var start = moment(scope.startDate).format('YYYY-MM-DD'), end = moment(scope.endDate).format('YYYY-MM-DD');
                    $(element).daterangepicker();// initialize date range picker
                    $(element).daterangepicker(
                        {
                            locale: {
                                format: 'YYYY-MM-DD'
                            },
                            startDate: start,
                            endDate: end
                        },
                        function(start, end, label) {
                            $timeout(
                                function(){
                                    scope.startDate = start._d;
                                    scope.endDate   = end._d;
                                    scope.initData();//load the values again once the date range is changed
                                }
                            );

                            //alert("A new date range was chosen: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                        });
                }
            });

        });

})();
