/**
 * Created by abdulmoiz on 1/23/2016.
 */
/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config/environment');

// Setup server
var app = express();
var router = express.Router();
var http = require('http');

var server = http.createServer(app);


if( process.env.NODE_ENV == 'development' ){
    //allowing CORS in development mode
    app.all('*', function(req, res, next) {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods','POST, GET, OPTIONS, PUT, DELETE');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
        next();
    });
}

require('./config/express')(app, router);
require('./routes')(app);

// Start server

server.listen(config.port, config.ip, function () {
    console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

process.on('uncaughtException', function(err){
    console.log('Uncaught Error: '+ err);
});

module.exports = app;
