/**
 * Created by abdulmoiz on 1/31/2016.
 */
'use strict';
var q = require('q');
var async = require('async');
var xeroService = require('../../modules/Xero');
var dataHelper  = require('../../helper/dataHelper');

var XeroCtrl = (function () {


    var getAccounts = function getActivity(date){
        var defer = q.defer();
        async.parallel(
            [
                function(cb){
                    xeroService.getActiveAccounts(function(err, resp){
                        if(err){
                            return cb({status:500, errorMsg:'Xero api error!', error: err});
                        }
                        var data  = resp && resp['Response'] ;
                        var accounts = (data && data["Accounts"] && data["Accounts"]["Account"]) || [];
                        cb(null, accounts);
                    });
                },
                function(cb){
                    xeroService.getBalanceSheet(date, function(err, resp){
                        if(err){
                            return cb({status:500, errorMsg:'Xero api error!', error: err});
                        }
                        var data  = resp && resp['Response'] ;
                        var balanceInfo = dataHelper.extractBalance(data);
                        cb(null, balanceInfo);
                    });
                }
            ],
            function(err, results){
                if(err){
                    return defer.reject(err);
                }
                var accounts = results[0].map(function(account){
                                                account['balance'] = results[1][account.AccountID] || 0;
                                                return account;
                                             });
                defer.resolve(accounts);
            }
        );
        return defer.promise;
    };


    var getPayments = function getPayments(startDate, endDate){
        var defer = q.defer();
        async. waterfall(
            [
                function(cb){
                    xeroService.getPayments(startDate, endDate, function(err, resp){
                        if(err){
                            return cb({status:500, errorMsg:'Xero api error!', error: err});
                        }
                        cb(null,  resp && resp['Response']);
                    });
                },
                function(data, cb){
                    var payments = (data && data["Payments"] && data["Payments"]["Payment"]) || [];
                    cb(null, payments);
                }
            ],
            function(err, payments){
                if(err){
                    defer.reject(err);
                }
                else{
                    defer.resolve(payments);
                }
            }
        );
        return defer.promise;
    };


    var getInvoices = function getInvoices(startDate, endDate){
        var defer = q.defer();
        async. waterfall(
            [
                function(cb){
                    xeroService.getInvoices(startDate, endDate, function(err, resp){
                        if(err){
                            return cb({status:500, errorMsg:'Xero api error!', error: err});
                        }
                        cb(null,  resp && resp['Response']);
                    });
                },
                function(data, cb){
                    var invoices = (data && data["Invoices"] && data["Invoices"]["Invoice"]) || [];
                    cb(null, invoices);
                }
            ],
            function(err, invoices){
                if(err){
                    defer.reject(err);
                }
                else{
                    defer.resolve(invoices);
                }
            }
        );
        return defer.promise;
    };


    return {
        getAccounts: getAccounts,
        getInvoices: getInvoices,
        getPayments: getPayments
    }

})();


module.exports = XeroCtrl;
